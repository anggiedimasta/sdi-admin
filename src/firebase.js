import firebase from 'firebase/app'
import '@firebase/auth'
import '@firebase/firestore'
import '@firebase/storage'

const config = {
	apiKey: "AIzaSyBjz-RFa0AhTgfBorV0LkJj87s8LSjcJCo",
	authDomain: "sdi-admin.firebaseapp.com",
	databaseURL: "https://sdi-admin.firebaseio.com",
	projectId: "sdi-admin",
	storageBucket: "sdi-admin.appspot.com",
	messagingSenderId: "915712456026"
}
firebase.initializeApp(config)

export default firebase