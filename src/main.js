// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import PortalVue from 'portal-vue'
import VueIziToast from 'vue-izitoast'
import vueNumeralFilterInstaller from 'vue-numeral-filter'
import VueProgressBar from 'vue-progressbar'
import App from './App'
import router from './router'
import store from './store'
import './auth'

Vue.config.productionTip = false

const toast_setting = {
	class: 'rounded-full, cursor-pointer',
	closeOnClick: true,
	closeOnEscape: true,
	displayMode: 'replace',
	position: 'topRight',
	theme: 'light',
	timeout: 3000,
	transitionIn: 'bounceInDown',
	transitionOut: 'fadeOutRight'
}
const progressbar_setting = {
	color: 'rgb(143, 255, 199)',
	transition: {
		speed: '0',
		opacity: '0',
		termination: 0
	},
}

Vue.use(PortalVue)
Vue.use(VueIziToast, toast_setting)
Vue.use(vueNumeralFilterInstaller, {
	locale: 'id'
})
Vue.use(VueProgressBar, progressbar_setting)

/* eslint-disable no-new */
new Vue({
	components: {
		App,
	},
	el: '#app',
	router,
	store,
	template: '<App/>',
})