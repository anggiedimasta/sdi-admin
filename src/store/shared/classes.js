import firebase from '@/firebase'
import _ from 'underscore'

const firestore = firebase.firestore()

const settings = {
	timestampsInSnapshots: true
}
firestore.settings(settings)

const state = {
	class_members: null,
	classes_by_student: null,
	classes_by_teacher: null,
}

const getters = {
	class_members: ({
		class_members
	}) => class_members,
	classes_by_student: ({
		classes_by_student
	}) => classes_by_student,
	classes_by_teacher: ({
		classes_by_teacher
	}) => classes_by_teacher,
}

const mutations = {
	SET_CLASSES_MEMBERS(state, class_members) {
		state.class_members = class_members
	},
	SET_CLASSES_BY_STUDENT(state, classes_by_student) {
		state.classes_by_student = classes_by_student
	},
	SET_CLASSES_BY_TEACHER(state, classes_by_teacher) {
		state.classes_by_teacher = classes_by_teacher
	},
}

const actions = {
	getClassMembers(store, payload) {
		const progress = this._vm.$Progress
		const toast = this._vm.toast
		let members = {}

		return new Promise((resolve, reject) => {
			progress.start()
			const classMembersDocRef = firestore.collection('/years/' + payload.year + '/classes/' + payload.class + '/semesters/' + payload.semester + '/members')
			classMembersDocRef.get()
				.then(function (snapshot) {
					progress.set(30)
					snapshot.docs.forEach(member => {
						const studentDocRef = firestore.doc(member.data().ref.path)
						progress.set(66)
						studentDocRef.get()
							.then(function (student) {
								members[student.id] = student.data()
							})
							.catch(function (error) {
								const message = 'Gagal mendapatkan data siswa.'
								toast.error(message, 'Terjadi kesalahan!')
								reject(error)
							})
						progress.set(80)
					})
					store.commit('SET_CLASSES_MEMBERS', members)
					progress.finish()
					resolve()
				})
				.catch(function (error) {
					const message = 'Gagal mendapatkan data anggota kelas.'
					toast.error(message, 'Terjadi kesalahan!')
					progress.fail()
					reject(error)
				})
		})
	},
	async getClassesByStudent(store, payload) {
		let temp_courses = {}
		const studentClassesDocRef = await firestore.collection('students').doc(payload.username)
		studentClassesDocRef.get()
			.then(function (student) {
				let classes_data = student.data().classes
				_.map(classes_data, function (classe, year) {
					_.map(classe, function (semester, class_key) {
						_.map(semester, function (course, semester_key) {
							temp_courses[_.keys(course)[0]] = {
								class: class_key,
								semester: semester_key,
								year: year
							}
						})
					})
				})
				store.dispatch('courses/getCourses', temp_courses, {
					root: true
				})
				store.commit('SET_CLASSES_BY_STUDENT', classes_data)
			})
			.catch(function (error) {
				console.log("Error getting document:", error)
			})
	},
	getClassesByTeacher(store, payload) {
		const toast = this._vm.toast
		let classes = []

		return new Promise((resolve, reject) => {
			const teacherClassesDocRef = firestore.collection('teachers').doc(payload.username)
			teacherClassesDocRef.get()
				.then(function (querySnapshot) {
					const homeroom_classes = querySnapshot.data().homeroom_classes
					_.each(homeroom_classes, function (classe, key_classe) {
						const yearClassesDocRef = firestore.collection(classe.path + '/semesters')
						yearClassesDocRef.get()
							.then(function (snapshot) {
								snapshot.forEach(semester => {
									let classes_data = {}
									classes_data.name = key_classe
									classes_data.year = parseInt(classe.path.slice(6, 10))
									const yearClassMembersDocRef = firestore.collection(classe.path + '/semesters/' + semester.id + '/members')
									yearClassMembersDocRef.get()
										.then(function (members) {
											let count = 0
											members.forEach(() => {
												count++
											})
											classes_data.semester = semester.id
											classes_data.total_students = count
											classes.push(classes_data)
										})
										.catch(function (error) {
											const message = 'Gagal mendapatkan data anggota kelas.'
											toast.error(message, 'Terjadi kesalahan!')
											reject(error)
										})
								})
							})
							.catch(function (error) {
								const message = 'Gagal mendapatkan data kelas.'
								toast.error(message, 'Terjadi kesalahan!')
								reject(error)
							})
					})
					resolve()
				})
				.catch(function (error) {
					const message = 'Gagal mendapatkan data kelas.'
					toast.error(message, 'Terjadi kesalahan!')
					reject(error)
				})
		}).then(() => {
			store.commit('SET_CLASSES_BY_TEACHER', classes)
		})
	},
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}