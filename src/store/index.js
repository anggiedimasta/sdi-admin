import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import shared from './shared'
import user from './user'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
	storage: window.localStorage,
	supportCircular: true,
	modules: ['shared', 'user'],
})

const store = new Vuex.Store({
	modules: {
		shared,
		user
	},
	plugins: [vuexLocal.plugin],
})

export default store