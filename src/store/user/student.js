import firebase from '@/firebase'
import _ from 'underscore'

const firestore = firebase.firestore()
const settings = {
	timestampsInSnapshots: true
}
firestore.settings(settings)

const state = {
}

const getters = {
}

const mutations = {
}

const actions = {
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}