import firebase from '@/firebase'
import router from '@/router'

const firestore = firebase.firestore()
const storage = firebase.storage().ref()

const settings = {
	timestampsInSnapshots: true
}
firestore.settings(settings)

const state = {
	settings: null,
	upload_progress: 0,
	user: null,
}

const getters = {
	settings: ({
		settings
	}) => settings,
	upload_progress: ({
		upload_progress
	}) => upload_progress,
	user: ({
		user
	}) => user
}

const mutations = {
	SET_METADATA(state, metadata) {
		state.user.metadata = metadata
	},
	SET_SETTINGS(state, settings) {
		state.settings = settings
	},
	SET_UPLOAD_PROGRESS(state, progress) {
		state.upload_progress = progress
	},
	SET_USER(state, user) {
		state.user = user.profile
		state.user.role = user.role
		state.user.username = user.username
		state.user.metadata = user.metadata
		state.user.signed_in = true
	},
}

const actions = {
	async setBiodata(store, payload) {
		const progress = this._vm.$Progress
		const toast = this._vm.toast
		progress.start()
		let data = {}
		data[`profile.${payload.key}`] = payload.data
		try {
			await firestore.collection(payload.role).doc(payload.username).update(data)
			progress.finish()
			const message = 'Berhasil memperbarui data <span class="font-bold">' + payload.label.toUpperCase() + '<//span>.'
			toast.success(message, 'Sukses!')
		} catch (error) {
			progress.fail()
			const message = 'Gagal memperbarui data <span class="font-bold">' + payload.label.toUpperCase() + '<//span>.'
			toast.error(message, 'Kesalahan!')
		}
	},
	async setPhoto(store, payload) {
		const toast = this._vm.toast
		let metadata = {
			'download_url': '',
			'file_name': '',
			'file_size': 0
		}

		if (payload.isUpload) {
			metadata.file_name = payload.file.name
			metadata.file_size = payload.file.size
			const path = store.state.user.role + '/' + store.state.user.username + '/profile/' + payload.file.name
			const upload_task = storage.child(path).put(payload.file)
			upload_task.on('state_changed',
				sp => {
					store.commit('SET_UPLOAD_PROGRESS', Math.floor(sp.bytesTransferred / sp.totalBytes * 100))
				},
				null,
				() => {
					upload_task.snapshot.ref.getDownloadURL().then(downloadURL => {
						metadata.download_url = downloadURL
						store.commit('SET_UPLOAD_PROGRESS', 0)
						const message = 'Berhasil mengunggah foto.'
						toast.success(message, 'Sukses!')
					})
				}
			)

			let firestore_data = {}
			firestore_data[`profile.photo`] = path
			await firestore.collection(store.state.user.role).doc(store.state.user.username).update(firestore_data)
		} else {
			const default_path = '/default/profile/portrait-default.jpg'
			const path = store.state.user.role + '/' + store.state.user.username + '/profile/' + store.state.user.metadata.file_name

			metadata.file_name = 'portrait-default.jpg'
			metadata.file_size = 5869

			await storage.child(path).delete().then(() => {
				const message = 'Berhasil menghapus foto.'
				toast.success(message, 'Sukses!')
			})

			storage.child(default_path).getDownloadURL().then(downloadURL => {
				metadata.download_url = downloadURL
			}, error => {
				console.error(error)
			})

			let default_data = {}
			default_data[`profile.photo`] = default_path
			await firestore.collection(store.state.user.role).doc(store.state.user.username).update(default_data)
		}

		store.commit('SET_METADATA', metadata)
	},
	async setSettings(store, role) {
		const settingsDocRef = await firestore.collection('settings').doc(role)
		settingsDocRef.get()
			.then(function (doc) {
				if (doc.exists) {
					store.commit('SET_SETTINGS', doc.data())
					switch (store.state.user.role) {
						case 'administrators':
							router.push({
								name: 'Administrator',
								params: {
									status: 'sign-in-success'
								}
							})
							break
						case 'students':
							router.push({
								name: 'Student',
								params: {
									status: 'sign-in-success'
								}
							})
							break
						case 'teachers':
							router.push({
								name: 'Teacher',
								params: {
									status: 'sign-in-success'
								}
							})
							break
						default:
							router.push({
								name: 'Index'
							})
							break
					}
				} else {
					console.log("No such document!")
				}
			})
			.catch(function (error) {
				console.log("Error getting document:", error)
			})
	},
	async signIn(store, payload) {
		const progress = this._vm.$Progress
		const toast = this._vm.toast

		progress.start()
		const roleDocRef = await firestore.collection(payload.selected_role).doc(payload.username)
		roleDocRef.get()
			.then(function (doc) {
				progress.set(30)
				const email = doc.data().email
				let data = {
					'metadata': {
						'download_url': '',
						'file_name': '',
						'file_size': 0
					},
					'profile': doc.data().profile,
					'role': payload.selected_role,
					'username': payload.username
				}

				firebase.auth().signInWithEmailAndPassword(email, payload.password)
					.then(
						user => {
							storage.child(doc.data().profile.photo).getDownloadURL().then(downloadURL => {
								data.metadata.download_url = downloadURL
							}, error => {
								console.error(error)
							})
							progress.set(66)

							storage.child(doc.data().profile.photo).getMetadata().then(metadata => {
								data.metadata.file_name = metadata.name
								data.metadata.file_size = metadata.size
							}).catch(function (error) {
								console.error(error)
							})
							progress.set(80)

							store.dispatch('setSettings', payload.selected_role)
							store.commit('SET_USER', data)
						}
					)
					.catch(
						error => {
							progress.fail()
							switch (error.code) {
								case 'auth/network-request-failed':
									toast.error('Kondisi jaringan tidak stabil.', 'Kesalahan!')
									break
								case 'auth/too-many-requests':
									toast.error('Server sibuk, ulangi beberapa saat lagi.', 'Kesalahan!')
									break
								case 'auth/user-disabled':
									toast.error('Akun telah dibekukan.', 'Kesalahan!')
									break
								case 'auth/wrong-password':
									toast.error('Kata sandi salah.', 'Kesalahan!')
									break
								case 'auth/wrong-username':
									toast.error('Nama pengguna salah.', 'Kesalahan!')
									break
								default:
									toast.error('', 'Kesalahan!')
									break
							}
						}
					)
			})
			.catch(
				error => {
					progress.fail()
					toast.error('Nama pengguna salah.', 'Kesalahan!')
				}
			)
	},
	async signOut(store) {
		const progress = this._vm.$Progress
		const toast = this._vm.toast

		progress.start()
		try {
			await firebase.auth().signOut().then(() => {
				store.state.settings = null
				store.state.upload_progress = 0
				store.state.user = null
				localStorage.clear()
				router.push({
					name: 'SignIn'
				})
				progress.finish()
				toast.success('Berhasil keluar dari sistem.', 'Sukses!')
			})
		} catch (error) {
			progress.fail()
			toast.error('Gagal keluar dari sistem.', 'Kesalahan!')
		}
	},
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}