import Vue from 'vue'
import Router from 'vue-router'
import VueRouterBackButton from 'vue-router-back-button'
import store from '@/store'

import SignIn from '@/components/SignIn'
const SignUp = () => import( /* webpackChunkName: "signup" */ '@/components/SignUp')

const Administrator = () => import( /* webpackChunkName: "administrator" */ '@/components/dashboard/Administrator')
const AdministratorAcademics = () => import( /* webpackChunkName: "administrator-academics" */ '@/components/dashboard/administrator/Academics')
const AdministratorAcademicsClasses = () => import( /* webpackChunkName: "administrator-academics-classes" */ '@/components/dashboard/administrator/academics/Classes')
const AdministratorAcademicsClass = () => import( /* webpackChunkName: "administrator-academics-class" */ '@/components/dashboard/administrator/academics/classes/Class')
const AdministratorAcademicsClassCourse = () => import( /* webpackChunkName: "administrator-academics-class-course" */ '@/components/dashboard/administrator/academics/classes/class/Course')
const AdministratorAcademicsCourses = () => import( /* webpackChunkName: "administrator-academics-courses" */ '@/components/dashboard/administrator/academics/Courses')
const AdministratorAcademicsCourse = () => import( /* webpackChunkName: "administrator-academics-course" */ '@/components/dashboard/administrator/academics/courses/Course')
const AdministratorAcademicsYears = () => import( /* webpackChunkName: "administrator-academics-years" */ '@/components/dashboard/administrator/academics/Years')
const AdministratorAcademicsYear = () => import( /* webpackChunkName: "administrator-academics-year" */ '@/components/dashboard/administrator/academics/years/Year')
const AdministratorTeacher = () => import( /* webpackChunkName: "administrator-teacher" */ '@/components/dashboard/administrator/teachers/Teacher')
const AdministratorTeachers = () => import( /* webpackChunkName: "administrator-teachers" */ '@/components/dashboard/administrator/Teachers')
const AdministratorStudent = () => import( /* webpackChunkName: "administrator-student" */ '@/components/dashboard/administrator/students/Student')
const AdministratorStudents = () => import( /* webpackChunkName: "administrator-students" */ '@/components/dashboard/administrator/Students')
const AdministratorDonations = () => import( /* webpackChunkName: "administrator-donations" */ '@/components/dashboard/administrator/Donations')

const Student = () => import( /* webpackChunkName: "student" */ '@/components/dashboard/Student')
const StudentBills = () => import( /* webpackChunkName: "student-bills" */ '@/components/dashboard/student/Bills')
const StudentDonations = () => import( /* webpackChunkName: "student-donations" */ '@/components/dashboard/student/Donations')
const StudentGrades = () => import( /* webpackChunkName: "student-grades" */ '@/components/dashboard/student/Grades')

const Teacher = () => import( /* webpackChunkName: "teacher" */ '@/components/dashboard/Teacher')
const TeacherClass = () => import( /* webpackChunkName: "teacher-class" */ '@/components/dashboard/teacher/classes/Class')
const TeacherClasses = () => import( /* webpackChunkName: "teacher-classes" */ '@/components/dashboard/teacher/Classes')
const TeacherFiles = () => import( /* webpackChunkName: "teacher-files" */ '@/components/dashboard/teacher/Files')
const TeacherCourse = () => import( /* webpackChunkName: "teacher-course" */ '@/components/dashboard/teacher/Course')
const TeacherStudent = () => import( /* webpackChunkName: "teacher-student" */ '@/components/dashboard/teacher/Student')

Vue.use(Router)

const router = new Router({
	mode: 'history',
	routes: [{
			name: 'Index',
			meta: {
				requiresAuth: false
			},
			path: '/',
			redirect: {
				name: 'SignIn'
			}
		},
		{
			component: Administrator,
			name: 'Administrator',
			meta: {
				pageTitle: 'Dasbor',
				requiresAuth: true,
				role: 'administrators',
			},
			path: '/administrator'
		},
		{
			component: AdministratorAcademics,
			name: 'AdministratorAcademics',
			meta: {
				pageTitle: 'Akademik',
				requiresAuth: true,
				role: 'administrators',
			},
			path: '/administrator/academics'
		},
		{
			component: AdministratorAcademicsClass,
			name: 'AdministratorAcademicsClass',
			meta: {
				pageTitle: 'Kelas ',
				requiresAuth: true,
				role: 'administrators',
			},
			path: '/administrator/academics/class/:year/:class/:semester'
		},
		{
			component: AdministratorAcademicsClassCourse,
			name: 'AdministratorAcademicsClassCourse',
			meta: {
				pageTitle: 'Mata Pelajaran',
				requiresAuth: true,
				role: 'administrators',
			},
			path: '/administrator/academics/class/:year/:class/:semester/:course'
		},
		{
			component: AdministratorDonations,
			name: 'AdministratorDonations',
			meta: {
				pageTitle: 'Donasi',
				requiresAuth: true,
				role: 'administrators',
			},
			path: '/administrator/donations'
		},
		{
			component: AdministratorTeachers,
			name: 'AdministratorTeachers',
			meta: {
				pageTitle: 'Guru',
				requiresAuth: true,
				role: 'administrators',
			},
			path: '/administrator/teachers'
		},
		{
			component: AdministratorTeacher,
			name: 'AdministratorTeacher',
			meta: {
				pageTitle: 'Biodata',
				requiresAuth: true,
				role: 'administrators',
			},
			path: '/administrator/teacher/:username'
		},
		{
			component: AdministratorStudent,
			name: 'AdministratorStudent',
			meta: {
				pageTitle: 'Biodata',
				requiresAuth: true,
				role: 'administrators',
			},
			path: '/administrator/student/:nis'
		},
		{
			component: AdministratorStudents,
			name: 'AdministratorStudents',
			meta: {
				pageTitle: 'Murid',
				requiresAuth: true,
				role: 'administrators',
			},
			path: '/administrator/students'
		},
		{
			component: Student,
			name: 'Student',
			meta: {
				pageTitle: 'Biodata',
				requiresAuth: true,
				role: 'students',
			},
			path: '/student'
		},
		{
			component: StudentBills,
			name: 'StudentBills',
			meta: {
				pageTitle: 'Registrasi',
				requiresAuth: true,
				role: 'students',
			},
			path: '/student/bills'
		},
		{
			component: StudentGrades,
			name: 'StudentGrades',
			meta: {
				pageTitle: 'Akademik',
				requiresAuth: true
			},
			path: '/student/grades'
		},
		{
			component: StudentDonations,
			name: 'StudentDonations',
			meta: {
				pageTitle: 'Donasi',
				requiresAuth: true,
				role: 'students',
			},
			path: '/student/donations'
		},
		{
			component: Teacher,
			name: 'Teacher',
			meta: {
				pageTitle: 'Biodata',
				requiresAuth: true,
				role: 'teachers',
			},
			path: '/teacher'
		},
		{
			component: TeacherClasses,
			name: 'TeacherClasses',
			meta: {
				pageTitle: 'Kelas',
				requiresAuth: true,
				role: 'teachers',
			},
			path: '/teacher/classes'
		},
		{
			component: TeacherClass,
			name: 'TeacherClass',
			meta: {
				pageTitle: 'Kelas ',
				requiresAuth: true,
				role: 'teachers',
			},
			path: '/teacher/class/:year/:class/:semester'
		},
		{
			component: TeacherCourse,
			name: 'TeacherCourse',
			meta: {
				pageTitle: 'Mata Pelajaran',
				requiresAuth: true,
				role: 'teachers',
			},
			path: '/teacher/class/:year/:class/:semester/:course'
		},
		{
			component: TeacherFiles,
			name: 'TeacherFiles',
			meta: {
				pageTitle: 'Berkas',
				requiresAuth: true,
				role: 'teachers',
			},
			path: '/teacher/files'
		},
		{
			component: TeacherStudent,
			name: 'TeacherStudent',
			meta: {
				pageTitle: 'Biodata',
				requiresAuth: true,
				role: 'teachers',
			},
			path: '/teacher/student/:nis'
		},
		{
			component: SignIn,
			name: 'SignIn',
			meta: {
				pageTitle: 'Masuk',
				requiresAuth: false,
				role: 'guest',
			},
			path: '/signin'
		},
		{
			component: SignUp,
			name: 'SignUp',
			meta: {
				pageTitle: 'Daftar',
				requiresAuth: false,
				role: 'guest',
			},
			path: '/signup'
		},
	]
})

router.beforeEach((to, from, next) => {
	const pageTitle = to.matched.slice().reverse().find(record => record.meta.pageTitle)
	const user = store.getters['user/user']
	const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
	let default_component = 'SignIn'

	if (user) {
		default_component = user.role.substring(0, user.role.length - 1)
		default_component = default_component.replace(/^\w/, c => c.toUpperCase())
	}

	if (pageTitle) document.title = pageTitle.meta.pageTitle

	if (user) {
		if (requiresAuth) {
			if (to.meta.role === user.role) next()
			else {
				next({
					name: default_component
				})
			}
		} else {
			next({
				name: default_component
			})
		}
	} else next()
})

Vue.use(VueRouterBackButton, {
	router
})

export default router