import firebase from '@/firebase'
import _ from 'underscore'

const firestore = firebase.firestore()
const settings = {
	timestampsInSnapshots: true
}
firestore.settings(settings)

const state = {
	donations: null,
	donations_by_student: null,
}

const getters = {
	donations: ({
		donations
	}) => donations,
	donations_by_student: ({
		donations_by_student
	}) => donations_by_student,
}

const mutations = {
	SET_DONATIONS(state, donations) {
		state.donations = donations
	},
	SET_DONATIONS_BY_STUDENT(state, donations_by_student) {
		state.donations_by_student = donations_by_student
	},
}

const actions = {
	async getDonations(store) {
		await firestore.collection('donations').get()
			.then(donations => {
				let donationsData = {}
				donations.forEach(doc => {
					donationsData[doc.id] = doc.data()
				})
				store.commit('SET_DONATIONS', donationsData)
			})
			.catch(function (error) {
				console.log("Error getting documents: ", error)
			})
	},
	async getDonationsByStudent(store, payload) {
		await firestore.collection('students/' + payload.username + '/donations').get()
			.then(querySnapshot => {
				let donationsData = {}
				querySnapshot.forEach(doc => {
					donationsData[doc.id] = doc.data()
				})
				store.commit('SET_DONATIONS_BY_STUDENT', donationsData)
			})
			.catch(function (error) {
				console.log("Error getting documents: ", error)
			})
	},
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}