import Vue from 'vue'
import Vuex from 'vuex'

import bills from './bills'
import classes from './classes'
import courses from './courses'
import donations from './donations'
import settings from './settings'

Vue.use(Vuex)

const shared = new Vuex.Store({
	modules: {
		bills,
		classes,
		courses,
		donations,
		settings,
	},
})

export default shared