import firebase from '@/firebase'
import _ from 'underscore'

const firestore = firebase.firestore()
const settings = {
	timestampsInSnapshots: true
}
firestore.settings(settings)

const state = {
	balance_by_student: 0,
	bills_by_student: {},
	bills_years_by_student: [],
}

const getters = {
	balance_by_student: ({
		balance_by_student
	}) => balance_by_student,
	bills_by_student: ({
		bills_by_student
	}) => bills_by_student,
	bills_years_by_student: ({
		bills_years_by_student
	}) => bills_years_by_student,
}

const mutations = {
	SET_BALANCE_BY_STUDENT(state, balance_by_student) {
		state.balance_by_student = balance_by_student
	},
	SET_BILLS_BY_STUDENT(state, bills_by_student) {
		state.bills_by_student = bills_by_student
	},
	SET_BILLS_YEARS_BY_STUDENT(state, bills_years_by_student) {
		state.bills_years_by_student = bills_years_by_student
	},
}

const actions = {
	async getBillsByStudent(store, payload) {
		const studentBillsDocRef = await firestore.collection('bills').doc(payload.username)
		studentBillsDocRef.get()
			.then(student => {
				let selected_year_refs = null
				let bills_data = []
				if (!payload.year) {
					selected_year_refs = student.data().years[_.last(_.keys(student.data().years))]
				} else {
					selected_year_refs = student.data().years[payload.year]
				}
				_.each(selected_year_refs, ref => {
					ref.get()
						.then(transaction => {
							bills_data.push(transaction.data())
						})
				})
				store.commit('SET_BALANCE_BY_STUDENT', student.data().balance)
				store.commit('SET_BILLS_BY_STUDENT', bills_data)
			})
	},
	async getBillsYearsByStudent(store, payload) {
		let years = []
		const studentBillsDocRef = await firestore.collection('bills').doc(payload.username)
		studentBillsDocRef.get()
			.then(student => {
				_.map(student.data().years, (data, year) => {
					years.push(year)
				})
			})
		store.commit('SET_BILLS_YEARS_BY_STUDENT', years)
	},
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}