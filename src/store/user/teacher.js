import firebase from '@/firebase'
import _ from 'underscore'

const firestore = firebase.firestore()
const storage = firebase.storage().ref()

const settings = {
	timestampsInSnapshots: true
}
firestore.settings(settings)

const state = {
	student: null,
	student_settings: null,
	student_upload_progress: 0,
	teacher: null,
	teacher_courses: null,
	teacher_files: null,
	teacher_files_metadata: null,
	teacher_settings: null,
	teacher_upload_progress: 0,
	teachers: null,
}

const getters = {
	student: ({
		student
	}) => student,
	student_settings: ({
		student_settings
	}) => student_settings,
	student_upload_progress: ({
		student_upload_progress
	}) => student_upload_progress,
	teacher: ({
		teacher
	}) => teacher,
	teacher_courses: ({
		teacher_courses
	}) => teacher_courses,
	teacher_files: ({
		teacher_files
	}) => teacher_files,
	teacher_files_metadata: ({
		teacher_files_metadata
	}) => teacher_files_metadata,
	teacher_settings: ({
		teacher_settings
	}) => teacher_settings,
	teacher_upload_progress: ({
		teacher_upload_progress
	}) => teacher_upload_progress,
	teachers: ({
		teachers
	}) => teachers,
}

const mutations = {
	SET_STUDENT(state, student) {
		state.student = student.profile
		state.student.metadata = student.metadata
	},
	SET_STUDENT_METADATA(state, metadata) {
		state.student.metadata = metadata
	},
	SET_STUDENT_SETTINGS(state, student_settings) {
		state.student_settings = student_settings
	},
	SET_STUDENT_UPLOAD_PROGRESS(state, student_upload_progress) {
		state.student_upload_progress = student_upload_progress
	},
	SET_TEACHER(state, teacher) {
		state.teacher = teacher.profile
		state.teacher.classes = teacher.classes
		state.teacher.metadata = teacher.metadata
		state.teacher.username = teacher.username
	},
	SET_TEACHER_COURSES(state, teacher_courses) {
		state.teacher_courses = teacher_courses
	},
	SET_TEACHER_FILES(state, teacher_files) {
		state.teacher_files = teacher_files
	},
	SET_TEACHER_FILES_METADATA(state, teacher_files_metadata) {
		state.teacher_files_metadata = teacher_files_metadata
	},
	SET_TEACHER_SETTINGS(state, teacher_settings) {
		state.teacher_settings = teacher_settings
	},
	SET_TEACHER_UPLOAD_PROGRESS(state, teacher_upload_progress) {
		state.teacher_upload_progress = teacher_upload_progress
	},
	SET_TEACHERS(state, teachers) {
		state.teachers = teachers
	},
}

const actions = {
	deleteFile(store, payload) {
		const file_data = {
			username: payload.username,
			key: payload.type,
			data: ''
		}

		store.dispatch('setFile', file_data)
	},
	getStudent(store, payload) {
		const progress = this._vm.$Progress
		const toast = this._vm.toast

		progress.start()
		const studentDocRef = firestore.collection('students').doc(payload.nis)
		studentDocRef.get()
			.then(function (doc) {
				progress.set(30)
				let data = {
					'metadata': {
						'download_url': '',
						'file_name': '',
						'file_size': 0
					},
					'profile': doc.data().profile,
				}

				storage.child(doc.data().profile.photo).getDownloadURL().then(downloadURL => {
					data.metadata.download_url = downloadURL
				}, error => {
					progress.fail()
					toast.error('Berkas foto siswa tidak ditemukan.', 'Kesalahan!')
				})
				progress.set(66)

				storage.child(doc.data().profile.photo).getMetadata().then(metadata => {
					data.metadata.file_name = metadata.name
					data.metadata.file_size = metadata.size
				}, error => {
					progress.fail()
					toast.error('Data foto siswa tidak ditemukan.', 'Kesalahan!')
				})
				progress.set(80)

				store.dispatch('getStudentSettings')
				store.commit('SET_STUDENT', data)
			})
			.catch(function (error) {
				progress.fail()
				toast.error('Data siswa tidak ditemukan.', 'Kesalahan!')
			})
	},
	async getStudentSettings(store) {
		const progress = this._vm.$Progress
		const toast = this._vm.toast

		const studentSettingsDocRef = await firestore.collection('settings').doc('students')
		studentSettingsDocRef.get()
			.then(function (doc) {
				store.commit('SET_STUDENT_SETTINGS', doc.data())
				progress.finish()
			})
			.catch(function (error) {
				const message = 'Gagal mendapatkan data anggota kelas.'
				toast.error(message, 'Terjadi kesalahan!')
				progress.fail()
			})
	},
	getTeacher(store, payload) {
		const progress = this._vm.$Progress
		const toast = this._vm.toast

		return new Promise((resolve, reject) => {
			progress.start()
			const teacherDocRef = firestore.collection('teachers').doc(payload.username)
			teacherDocRef.get()
				.then(function (doc) {
					progress.set(30)
					let data = {
						'classes': [],
						'metadata': {
							'download_url': '',
							'file_name': '',
							'file_size': 0
						},
						'profile': doc.data().profile,
						'username': payload.username,
					}

					_.each(doc.data().homeroom_classes, function (classe, key_classe) {
						const yearClassesDocRef = firestore.collection(classe.path + '/semesters')
						yearClassesDocRef.get()
							.then(function (snapshot) {
								snapshot.forEach(semester => {
									let classes_data = {}
									classes_data.name = key_classe
									classes_data.year = parseInt(classe.path.slice(6, 10))
									const yearClassMembersDocRef = firestore.collection(classe.path + '/semesters/' + semester.id + '/members')
									yearClassMembersDocRef.get()
										.then(function (members) {
											let count = 0
											members.forEach(() => {
												count++
											})
											classes_data.semester = semester.id
											classes_data.total_students = count
											data.classes.push(classes_data)
										})
										.catch(function (error) {
											const message = 'Gagal mendapatkan data anggota kelas.'
											toast.error(message, 'Terjadi kesalahan!')
											reject(error)
										})
								})
							})
							.catch(function (error) {
								const message = 'Gagal mendapatkan data kelas.'
								toast.error(message, 'Terjadi kesalahan!')
								reject(error)
							})
					})

					storage.child(doc.data().profile.photo).getDownloadURL().then(downloadURL => {
						data.metadata.download_url = downloadURL
					}, error => {
						progress.fail()
						toast.error('Berkas foto guru tidak ditemukan.', 'Kesalahan!')
						reject(error)
					})
					progress.set(66)

					storage.child(doc.data().profile.photo).getMetadata().then(metadata => {
						data.metadata.file_name = metadata.name
						data.metadata.file_size = metadata.size
						resolve()
					}, error => {
						progress.fail()
						toast.error('Data foto guru tidak ditemukan.', 'Kesalahan!')
						reject(error)
					})
					progress.set(80)

					store.dispatch('getTeacherSettings').then(() => {
						store.commit('SET_TEACHER', data)
					})
				})
				.catch(function (error) {
					progress.fail()
					toast.error('Data guru tidak ditemukan.', 'Kesalahan!')
					reject(error)
				})
		})
	},
	getTeacherFiles(store, payload) {
		const toast = this._vm.toast

		return new Promise((resolve, reject) => {
			const teacherFilesDocRef = firestore.collection('teachers').doc(payload.username)
			teacherFilesDocRef.get()
				.then(function (querySnapshot) {
					store.commit('SET_TEACHER_FILES', querySnapshot.data().files)
					resolve()
				})
				.catch(function (error) {
					const message = 'Gagal mendapatkan data berkas guru.'
					toast.error(message, 'Terjadi kesalahan!')
					reject(error)
				})
		})
	},
	getTeacherFilesMetadata(store) {
		const toast = this._vm.toast
		let files_data = {}
		const default_data = {
			'name': '-',
			'size': '-',
			'updated': '-'
		}
		const teacher_files = store.getters.teacher_files

		return new Promise((resolve, reject) => {
			_.each(teacher_files, function (file, key_file) {
				if (file) {
					storage.child(file).getMetadata().then(function (metadata) {
						files_data[`${key_file}`] = {
							'name': metadata.name,
							'size': metadata.size,
							'updated': metadata.updated
						}
					}).catch(function (error) {
						const message = 'Gagal mendapatkan berkas guru.'
						toast.error(message, 'Terjadi kesalahan!')
						reject()
					})
				} else {
					files_data[`${key_file}`] = default_data
				}
			})
			resolve()
		}).then(() => {
			store.commit('SET_TEACHER_FILES_METADATA', files_data)
		})
	},
	getTeacherSettings(store) {
		const progress = this._vm.$Progress

		return new Promise((resolve, reject) => {
			const teacherSettingsDocRef = firestore.collection('settings').doc('teachers')
			teacherSettingsDocRef.get()
				.then(function (doc) {
					store.commit('SET_TEACHER_SETTINGS', doc.data())
					progress.finish()
					resolve()
				})
				.catch(function (error) {
					const message = 'Gagal mendapatkan data guru.'
					this._vm.toast.error(message, 'Terjadi kesalahan!')
					progress.fail()
					reject(error)
				})
		})
	},
	async getTeachers(store) {
		await firestore.collection('teachers').get()
			.then(querySnapshot => {
				let teachersData = {}
				querySnapshot.forEach(doc => {
					teachersData[doc.id] = doc.data()
				})
				store.commit('SET_TEACHERS', teachersData)
			})
			.catch(function (error) {
				console.log("Error getting documents: ", error)
			})
	},
	getTeacherCourses(store, payload) {
		const progress = this._vm.$Progress
		const toast = this._vm.toast
		let courses = []

		return new Promise((resolve, reject) => {
			progress.start()
			const teacherCoursesDocRef = firestore.collection('years').doc(payload.year + '/classes/' + payload.class + '/semesters/' + payload.semester)
			teacherCoursesDocRef.get()
				.then(function (querySnapshot) {
					progress.set(30)
					const courses_data = querySnapshot.data().courses
					_.each(courses_data, function (course, key_course) {
						const courseDocRef = firestore.doc(course.path)
						courseDocRef.get()
							.then(function (snapshot) {
								const data = {
									id: key_course,
									name: snapshot.data().name
								}
								courses.push(data)
							})
							.catch(function (error) {
								const message = 'Gagal mendapatkan data mata pelajaran.'
								toast.error(message, 'Terjadi kesalahan!')
								reject(error)
							})
						progress.set(66)
					})
					store.commit('SET_TEACHER_COURSES', courses)
					progress.finish()
					resolve()
				})
				.catch(function (error) {
					const message = 'Gagal mendapatkan data mata pelajaran.'
					toast.error(message, 'Terjadi kesalahan!')
					progress.fail()
					reject(error)
				})
		})
	},
	async setCourseEditSubGrade(store, payload) {
		const toast = this._vm.toast
		let data = {}
		data[`settings.${payload.subgrade}.percentage`] = parseInt(payload.data)
		data[`settings.${payload.subgrade}.edit`] = false

		try {
			await firestore.collection('courses').doc(payload.course +
				'/years/' +
				payload.year +
				'/classes/' +
				payload.class +
				'/semesters/' +
				payload.semester).update(data)
				.then(function () {
					const message = 'Berhasil memperbarui persentase ' + payload.label + '.'
					toast.success(message, 'Sukses!')
				})
		} catch (error) {
			const message = 'Gagal memperbarui persentase ' + payload.label + '.'
			toast.error(message, 'Terjadi kesalahan!')
		}
	},
	async setCourseEnableSubGrade(store, payload) {
		const toast = this._vm.toast
		let data = {}
		data[`settings.${payload.subgrade}.enabled`] = payload.data

		try {
			await firestore.collection('courses').doc(payload.course +
				'/years/' +
				payload.year +
				'/classes/' +
				payload.class +
				'/semesters/' +
				payload.semester).update(data)
				.then(function () {
					let status = 'mengaktifkan'
					if (payload.data === false) {
						status = 'menonaktifkan'
					}
					const message = 'Berhasil ' + status + ' ' + payload.label + '.'
					toast.success(message, 'Sukses!')
				})
		} catch (error) {
			const message = 'Gagal memperbarui pengaturan ' + payload.label + '.'
			toast.success(message, 'Terjadi kesalahan!')
		}
	},
	setFile(store, payload) {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				let firestore_data = {}
				firestore_data[`files.${payload.key}`] = payload.data

				let teacher_files_data = store.getters.teacher_files
				teacher_files_data[`${payload.key}`] = payload.data

				let teacher_files_metadata = store.getters.files_metadata

				const default_data = {
					'name': '-',
					'size': '-',
					'updated': '-'
				}

				try {
					firestore.collection('teachers').doc(payload.username).update(firestore_data)
					store.commit('SET_TEACHER_FILES', teacher_files_data)

					if (payload.data != '') {
						storage.child(payload.data).getMetadata().then(function (metadata) {
							teacher_files_metadata[`${payload.key}`] = {
								'name': metadata.name,
								'size': metadata.size,
								'updated': metadata.updated
							}
						}).catch(function (error) {
							console.log("Error getting file:", error)
						})
					} else {
						teacher_files_metadata[`${payload.key}`] = default_data
					}

					store.commit('SET_FILES_METADATA', teacher_files_metadata)
					resolve()
				} catch (error) {
					console.log(error)
					reject()
				}
			}, 1000)
		})
	},
	async setStudentBiodata(store, payload) {
		const toast = this._vm.toast
		let data = {}
		data[`profile.${payload.key}`] = payload.data

		try {
			await firestore.collection('students').doc(payload.nis).update(data)
			const message = 'Berhasil memperbarui data <span class="font-bold">' + payload.label.toUpperCase() + '<//span>.'
			toast.success(message, 'Sukses!')
		} catch (error) {
			console.log(error)
		}
	},
	async setStudentPhoto(store, payload) {
		const nis = store.state.student.nis.toString()
		const toast = this._vm.toast
		let metadata = {
			'download_url': '',
			'file_name': '',
			'file_size': 0
		}

		if (payload.isUpload) {
			metadata.file_name = payload.file.name
			metadata.file_size = payload.file.size
			const path = 'students/' + nis + '/profile/' + payload.file.name
			const upload_task = storage.child(path).put(payload.file)
			upload_task.on('state_changed',
				sp => {
					store.commit('SET_STUDENT_UPLOAD_PROGRESS', Math.floor(sp.bytesTransferred / sp.totalBytes * 100))
				},
				null,
				() => {
					upload_task.snapshot.ref.getDownloadURL().then(downloadURL => {
						metadata.download_url = downloadURL
						store.commit('SET_STUDENT_UPLOAD_PROGRESS', 0)
						const message = 'Berhasil mengunggah foto.'
						toast.success(message, 'Sukses!')
					})
				}
			)

			let firestore_data = {}
			firestore_data[`profile.photo`] = path
			await firestore.collection('students').doc(nis).update(firestore_data)
		} else {
			const default_path = '/default/profile/portrait-default.jpg'
			const path = 'students/' + nis + '/profile/' + store.state.student.metadata.file_name

			metadata.file_name = 'portrait-default.jpg'
			metadata.file_size = 5869

			await storage.child(path).delete().then(() => {
				const message = 'Berhasil menghapus foto.'
				toast.success(message, 'Sukses!')
			})

			storage.child(default_path).getDownloadURL().then(downloadURL => {
				metadata.download_url = downloadURL
			}, error => {
				console.error(error)
			})

			let default_data = {}
			default_data[`profile.photo`] = default_path
			await firestore.collection('students').doc(nis).update(default_data)
		}

		store.commit('SET_STUDENT_METADATA', metadata)
	},
	async setStudentSubGrade(store, payload) {
		const toast = this._vm.toast
		let data = {}
		data[`classes.${payload.year}.${payload.class}.${payload.semester}.${payload.course}.grade.${payload.subgrade}.value`] = parseInt(payload.data)
		data[`classes.${payload.year}.${payload.class}.${payload.semester}.${payload.course}.grade.${payload.subgrade}.edit`] = false

		try {
			await firestore.collection('students').doc(payload.nis.toString()).update(data)
			const message = 'Berhasil memperbarui nilai ' + payload.label + ' siswa.'
			toast.success(message, 'Sukses!')
		} catch (error) {
			const message = 'Gagal memperbarui nilai ' + payload.label + ' siswa.'
			toast.error(message, 'Terjadi kesalahan!')
		}
	},
	setTeacherFile(store, payload) {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				let firestore_data = {}
				firestore_data[`files.${payload.key}`] = payload.data

				let teacher_files_data = store.getters.teacher_files
				teacher_files_data[`${payload.key}`] = payload.data

				let teacher_files_metadata = store.getters.teacher_files_metadata

				const default_data = {
					'name': '-',
					'size': '-',
					'updated': '-'
				}

				try {
					firestore.collection('teachers').doc(payload.username).update(firestore_data)
					store.commit('SET_TEACHER_FILES', teacher_files_data)

					if (payload.data != '') {
						storage.child(payload.data).getMetadata().then(function (metadata) {
							teacher_files_metadata[`${payload.key}`] = {
								'name': metadata.name,
								'size': metadata.size,
								'updated': metadata.updated
							}
						}).catch(function (error) {
							console.log("Error getting file:", error)
						})
					} else {
						teacher_files_metadata[`${payload.key}`] = default_data
					}

					store.commit('SET_TEACHER_FILES_METADATA', teacher_files_metadata)
					resolve()
				} catch (error) {
					console.log(error)
					reject()
				}
			}, 1000)
		})
	},
	async setTeacherPhoto(store, payload) {
		const toast = this._vm.toast
		let metadata = {
			'download_url': '',
			'file_name': '',
			'file_size': 0
		}

		if (payload.isUpload) {
			metadata.file_name = payload.file.name
			metadata.file_size = payload.file.size
			const path = 'teachers/' + store.state.teacher.username + '/profile/' + payload.file.name
			const upload_task = storage.child(path).put(payload.file)
			upload_task.on('state_changed',
				sp => {
					store.commit('SET_TEACHER_UPLOAD_PROGRESS', Math.floor(sp.bytesTransferred / sp.totalBytes * 100))
				},
				null,
				() => {
					upload_task.snapshot.ref.getDownloadURL().then(downloadURL => {
						metadata.download_url = downloadURL
						store.commit('SET_TEACHER_UPLOAD_PROGRESS', 0)
						const message = 'Berhasil mengunggah foto.'
						toast.success(message, 'Sukses!')
					})
				}
			)

			let firestore_data = {}
			firestore_data[`profile.photo`] = path
			await firestore.collection('teachers').doc(store.state.teacher.username).update(firestore_data)
		} else {
			const default_path = '/default/profile/portrait-default.jpg'
			const path = 'teachers/' + store.state.teacher.username + '/profile/' + store.state.teacher.metadata.file_name

			metadata.file_name = 'portrait-default.jpg'
			metadata.file_size = 5869

			await storage.child(path).delete().then(() => {
				const message = 'Berhasil menghapus foto.'
				toast.success(message, 'Sukses!')
			})

			storage.child(default_path).getDownloadURL().then(downloadURL => {
				metadata.download_url = downloadURL
			}, error => {
				console.error(error)
			})

			let default_data = {}
			default_data[`profile.photo`] = default_path
			await firestore.collection('teachers').doc(store.state.teacher.username).update(default_data)
		}

		store.commit('SET_TEACHER_METADATA', metadata)
	},
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}