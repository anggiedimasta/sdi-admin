import firebase from '@/firebase'
import _ from 'underscore'

const firestore = firebase.firestore()
const settings = {
	timestampsInSnapshots: true
}
firestore.settings(settings)

const state = {
	courses: null,
}

const getters = {
	courses: ({
		courses
	}) => courses,
}

const mutations = {
	SET_COURSE(state, course) {
		state.courses[course.id] = course
	},
	SET_COURSES(state, courses) {
		state.courses = courses
	},
}

const actions = {
	getCourse(store, payload) {
		const toast = this._vm.toast
		const studentCourseDocRef = firestore.collection('courses').doc(payload.course)
		studentCourseDocRef.get()
			.then(function (doc) {
				let data = {
					'id': doc.id,
					'name': doc.data().name,
					'settings': {},
				}

				const studentCourseSettingsDocRef = firestore.collection('courses')
					.doc(payload.course + '/years/' +
						payload.year + '/classes/' +
						payload.class + '/semesters/' +
						payload.semester
					)
				studentCourseSettingsDocRef.get()
					.then(function (doc2) {
						data.settings = doc2.data().settings
						store.commit('SET_COURSE', data)
					})
					.catch(function (error) {
						const message = 'Gagal mendapatkan data mata pelajaran.'
						toast.error(message, 'Terjadi kesalahan!')
					})
			})
			.catch(function (error) {
				const message = 'Gagal mendapatkan data mata pelajaran.'
				toast.error(message, 'Terjadi kesalahan!')
			})
	},
	getCourses(store) {
		let courses = {}

		return new Promise((resolve, reject) => {
			const coursesDocRef = firestore.collection('courses')
			coursesDocRef.get()
				.then(function (snap_courses) {
					snap_courses.forEach(course => {
						courses[course.id] = {
							colors: course.data().colors,
							name: course.data().name,
							years: {}
						}

						let yearsDocRef = firestore.collection(
							'courses/' +
							course.id +
							'/years'
						)
						yearsDocRef.get()
							.then(function (snap_years) {
								snap_years.forEach(year => {
									courses[course.id].years[year.id] = {
										classes: {}
									}

									let classesDocRef = firestore.collection(
										'courses/' +
										course.id +
										'/years/' +
										year.id +
										'/classes'
									)

									classesDocRef.get()
										.then(function (snap_classes) {
											snap_classes.forEach(classe => {
												courses[course.id].years[year.id].classes[classe.id] = {
													semesters: {}
												}

												let semestersDocRef = firestore.collection(
													'courses/' +
													course.id +
													'/years/' +
													year.id +
													'/classes/' +
													classe.id +
													'/semesters'
												)

												semestersDocRef.get()
													.then(function (snap_semesters) {
														snap_semesters.forEach(semester => {
															courses[course.id].years[year.id].classes[classe.id].semesters[semester.id] = {
																average_grade: ''
															}
															courses[course.id].years[year.id].classes[classe.id].semesters[semester.id].average_grade = semester.data().average_grade
														})
													})
											})
										})
								})
							})
					})
				})
				.catch(function (error) {
					const message = 'Gagal mendapatkan data mata pelajaran.'
					this._vm.toast.error(message, 'Terjadi kesalahan!')
					reject(error)
				})

			store.commit('SET_COURSES', courses)
			resolve()
		})
	},
	// getCourses(store, payload) {
	// 	let coursesData = {}
	// 	firestore.collection('courses').get()
	// 		.then(function (courses) {
	// 			courses.forEach(function (doc) {
	// 				coursesData[doc.id] = {
	// 					name: '',
	// 					settings: {},
	// 					teacher: '',
	// 				}
	// 				coursesData[doc.id].name = doc.data().name
	// 			})
	// 			_.each(payload, function (course, key) {
	// 				let path = key + '/years/' + course.year + '/classes/' + course.class + '/semesters/' + course.semester
	// 				let courseDocRef = firestore.collection('courses').doc(path)
	// 				courseDocRef.get()
	// 					.then(function (courseDoc) {
	// 						coursesData[key].teacher = courseDoc.data().teacher
	// 						coursesData[key].settings = courseDoc.data().settings
	// 					})
	// 					.catch(function (error) {
	// 						console.log("Error getting document:", error)
	// 					})
	// 			})
	// 			store.commit('SET_COURSES', coursesData)
	// 		})
	// 		.catch(function (error) {
	// 			console.log("Error getting documents: ", error)
	// 		})
	// },
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}