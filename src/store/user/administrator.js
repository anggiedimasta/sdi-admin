import firebase from '@/firebase'
import _ from 'underscore'

const firestore = firebase.firestore()
const storage = firebase.storage().ref()

const settings = {
	timestampsInSnapshots: true
}
firestore.settings(settings)

const state = {
}

const getters = {
}

const mutations = {
	SET_TEACHER_METADATA(state, metadata) {
		state.teacher.metadata = metadata
	},
}

const actions = {
	getTeachers(store) {
		const progress = this._vm.$Progress
		const toast = this._vm.toast
		let teachers = {}

		return new Promise((resolve, reject) => {
			progress.start()
			const teachersDocRef = firestore.collection('teachers')
			teachersDocRef.get()
				.then(function (snap_teachers) {
					progress.set(30)
					snap_teachers.forEach(teacher => {
						teachers[teacher.id] = {
							address: teacher.data().profile.address,
							fullname: teacher.data().profile.fullname,
							nik: teacher.data().profile.nik,
							photo: '',
						}
						progress.set(66)
						storage.child(teacher.data().profile.photo).getDownloadURL().then(downloadURL => {
							teachers[teacher.id].photo = downloadURL
							store.commit('SET_TEACHERS', teachers)
							resolve()
						}, error => {
							toast.error('Berkas foto guru tidak ditemukan.', 'Kesalahan!')
							reject(error)
						})
						progress.set(80)
					})
					progress.finish()
				})
				.catch(function (error) {
					const message = 'Gagal mendapatkan data guru.'
					toast.error(message, 'Terjadi kesalahan!')
					progress.fail()
					reject(error)
				})
		})
	},
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}